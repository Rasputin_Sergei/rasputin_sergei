const startValue = 3;

let currentValue = startValue;
let answer = startValue;

while (currentValue !== 1) {
  answer = answer * (currentValue - 1);
  currentValue--; 
}

console.log(startValue + '! = ' + answer);